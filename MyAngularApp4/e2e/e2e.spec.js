
describe('Fun with numbers', function () {

    it('should return 4', function () {
        expect(2 + 2).toEqual(4);
    });

    it('should return 0', function () {
        expect(4 - 4).toEqual(0);
    });

    it('should be true', function () {
        expect(3 > 0).toBe(true);
    });

});

describe('Hello world', function () {
    var message = 'AngularJS';
    var input = element.all(by.model('myModel')).first();

    beforeEach(function () {
        browser.get('http://localhost:3000');
    });

    it('should have title', function () {
        expect(browser.getTitle())
            .toEqual('Hello World AngularJS application');
    });

    it('should be World', function () {
        expect(input.getAttribute('value')).toEqual('World');
    });

    it('should be Hello World!', function () {
        expect(element.all(by.binding('myModel')).first().getText())
            .toEqual('Hello World!');
    });

    it('should update greeting when data is entered in the input field',
        function () {
            input.clear().then(function () {
                input.sendKeys(message);
                expect(element.all(by.binding('myModel')).first().getText())
                    .toEqual('Hello ' + message + '!');
            });
        });

});

describe('Goodbye Everyone', function () {
    var message = 'AngularJS';
    var input = element.all(by.model('myModel')).get(1);

    beforeEach(function () {
        browser.get('http://localhost:3000');
    });

    it('should be Everyone', function () {
        expect(input.getAttribute('value'))
            .toEqual('Everyone');
    });

    it('should be Goodbye Everyone!', function () {
        expect(element.all(by.binding('myModel')).get(1).getText())
            .toEqual('Goodbye Everyone!');
    });

    it('should update greeting when data is entered in the input field',
        function () {
            input.clear().then(function () {
                input.sendKeys(message);
                expect(element.all(by.binding('myModel')).get(1).getText())
                    .toEqual('Goodbye ' + message + '!');
            });
        });

});