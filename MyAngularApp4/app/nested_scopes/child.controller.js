(function () {

    angular.module('app')
        .controller('ChildController', ChildController);

    ChildController.$inject = ['$scope'];

    function ChildController($scope) {
        $scope.myModel = 'Child controller\'s myModel';
        $scope.onlyChild = 'Child\'s variable';
    }

})();