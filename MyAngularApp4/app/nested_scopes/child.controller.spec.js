describe('ChildController tests', function () {
    var parentScope, childScope;
    var ChildController;

    beforeEach(function () {
        module('app');

        inject(function ($rootScope, $controller) {
            parentScope = $rootScope.$new();
            $controller('ParentController', { $scope: parentScope });
            childScope = parentScope.$new();
            ChildController = $controller('ChildController', { $scope: childScope });
        });
    });

    it('should be Child controller\'s myModel', function () {
        expect(childScope.myModel).toEqual('Child controller\'s myModel');
    });

    it('should be Child\'s variable', function () {
        expect(childScope.onlyChild).toEqual('Child\'s variable');
    });

    it('should be Parent\'s variable', function () {
        expect(childScope.onlyParent).toEqual('Parent\'s variable');
    });

    it('should be Parent controller\'s myModel', function () {
        expect(childScope.$parent.myModel).toEqual('Parent controller\'s myModel');
    });
});