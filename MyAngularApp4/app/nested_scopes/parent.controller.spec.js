describe('ParentController tests', function () {
    var scope;
    var ParentController;

    beforeEach(function () {
        module('app');

        inject(function ($controller) {
            scope = {};
            ParentController = $controller('ParentController', { $scope: scope });
        });
    });

    it('should be Parent controller\'s myModel', function () {
        expect(scope.myModel).toEqual('Parent controller\'s myModel');
    });

    it('should be Parent\'s variable', function () {
        expect(scope.onlyParent).toEqual('Parent\'s variable');
    });
});