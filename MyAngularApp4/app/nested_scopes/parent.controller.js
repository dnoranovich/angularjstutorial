(function () {

    angular.module('app')
        .controller('ParentController', ParentController);

    ParentController.$inject = ['$scope'];

    function ParentController($scope) {
        $scope.myModel = 'Parent controller\'s myModel';
        $scope.onlyParent = 'Parent\'s variable';
    }

})();