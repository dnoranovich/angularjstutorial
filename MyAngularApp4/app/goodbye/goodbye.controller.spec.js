describe('GoodbyeController tests', function () {
    var GoodbyeController;
    var scope;

    beforeEach(function () {
        module('app');

        inject(function ($controller) {
            scope = {};
            GoodbyeController = $controller('GoodbyeController', { $scope: scope });
        });

    });

    if ('should be Everyone', function () {
        expect(scope.myModel).toEqual('Everyone');
    });

});