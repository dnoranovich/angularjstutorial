(function(){
    angular.module('app')
    .controller('GoodbyeController', GoodbyeController);

    GoodbyeController.$inject = ['$scope'];

    function GoodbyeController($scope){
        $scope.myModel = 'Everyone';
    }
})();