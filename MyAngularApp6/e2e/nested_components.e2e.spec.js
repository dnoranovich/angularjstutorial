describe('Nested Components', function () {
    beforeEach(function () {
        browser.get('http://localhost:3000');
    });

    it('should be Parent\'s variable', function () {
        expect(element(by.binding('parent.onlyParent')).getText())
            .toEqual('Parent\'s variable');
    });

    it('should be Parent controller\'s myModel', function () {
        expect(element(by.binding('parent.myModel')).getText())
            .toEqual('Parent controller\'s myModel');
    });

    it('should be Child\'s variable', function () {
        expect(element(by.binding('child.onlyChild')).getText())
            .toEqual('Child\'s variable');
    });

    it('should be Child controller\'s myModel', function () {
        expect(element(by.binding('child.myModel')).getText())
            .toEqual('Child controller\'s myModel');
    });

    it('should be Parent\'s variable', function () {
        expect(element(by.binding('child.onlyParent')).getText())
            .toEqual('Parent\'s variable');
    });

    it('should be Parent controller\'s myModel', function () {
        expect(element(by.binding('child.parentMyModel')).getText())
            .toEqual('Parent controller\'s myModel');
    });
});