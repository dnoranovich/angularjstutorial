(function () {
    'use strict'

    angular.module('app')
        .component('parentComponent', {
            templateUrl: 'app/nested_scopes/parent-component.html',
            controller: ParentController,
            controllerAs: 'parent'
        });

    function ParentController() {
        var ctrl = this;

        ctrl.myModel = 'Parent controller\'s myModel';
        ctrl.onlyParent = 'Parent\'s variable';
    }

})();