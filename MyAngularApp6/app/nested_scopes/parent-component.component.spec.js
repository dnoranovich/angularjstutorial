describe('ParentController tests', function () {
    var ParentController;

    beforeEach(function () {
        module('app');

        inject(function ($componentController) {
            ParentController = $componentController('parentComponent');
        });
    });

    it('should be Parent controller\'s myModel', function () {
        expect(ParentController.myModel).toEqual('Parent controller\'s myModel');
    });

    it('should be Parent\'s variable', function () {
        expect(ParentController.onlyParent).toEqual('Parent\'s variable');
    });
});