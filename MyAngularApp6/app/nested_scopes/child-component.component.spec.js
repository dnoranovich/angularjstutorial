describe('ChildController tests', function () {
    var ChildController;

    beforeEach(function () {
        module('app');

        inject(function ($componentController) {
            var bindings = {
                parentMyModel: 'Parent MyModel',
                onlyParent: 'Only Parent'
            };
            ChildController = $componentController('childComponent', null, bindings);
        });
    });

    it('should be Child controller\'s myModel', function () {
        expect(ChildController.myModel).toEqual('Child controller\'s myModel');
    });

    it('should be Child\'s variable', function () {
        expect(ChildController.onlyChild).toEqual('Child\'s variable');
    });

    it('should be Parent MyModel', function () {
        expect(ChildController.parentMyModel).toEqual('Parent MyModel');
    });

    it('should be Only Parent', function () {
        expect(ChildController.onlyParent).toEqual('Only Parent');
    });

});