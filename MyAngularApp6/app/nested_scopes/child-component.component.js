(function () {

    angular.module('app')
        .component('childComponent', {
            templateUrl: 'app/nested_scopes/child-component.html',
            controller: ChildController,
            controllerAs: 'child',
            bindings: {
                parentMyModel: '<',
                onlyParent: '<'
            }
        });


    function ChildController() {
        var ctrl = this;

        ctrl.myModel = 'Child controller\'s myModel';
        ctrl.onlyChild = 'Child\'s variable';
    }

})();