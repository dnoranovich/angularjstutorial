(function () {

  angular.module('app')
    .component('helloComponent', {
      templateUrl: 'app/hello/hello-component.html',
      controller: HelloController
    });

  function HelloController() {
    var ctrl = this;

    ctrl.myModel = 'World';
  }

})();