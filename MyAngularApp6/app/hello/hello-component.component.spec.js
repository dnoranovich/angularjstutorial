"use strict"

describe('HelloController tests', function () {
    var HelloController;

    beforeEach(function () {
        module('app');

        inject(function ($componentController) {
            HelloController = $componentController('helloComponent');
        });

    });

    it('should be World', function () {
        expect(HelloController.myModel).toEqual('World');
    });
});