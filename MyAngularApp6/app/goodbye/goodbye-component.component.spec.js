describe('GoodbyeController tests', function () {
    var GoodbyeController;

    beforeEach(function () {
        module('app');

        inject(function ($componentController) {
            GoodbyeController = $componentController('goodbyeComponent');
        });

    });

    it('should be Everyone', function () {
        expect(GoodbyeController.myModel).toEqual('Everyone');
    });

});