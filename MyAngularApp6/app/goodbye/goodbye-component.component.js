(function () {
    
    angular.module('app')
        .component('goodbyeComponent', {
            templateUrl: 'app/goodbye/goodbye-component.html',
            controller: GoodbyeController,
            controllerAs: 'goodbye'
        });

    function GoodbyeController() {
        var ctrl = this;

        ctrl.myModel = 'Everyone';
    }
})();