(function () {

    angular.module('app')
        .controller('ParentController', ParentController);

    function ParentController() {
        var vm = this;

        vm.myModel = 'Parent controller\'s myModel';
        vm.onlyParent = 'Parent\'s variable';
    }

})();