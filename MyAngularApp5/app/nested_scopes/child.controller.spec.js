describe('ChildController tests', function () {
    var scope;

    beforeEach(function () {
        module('app');

        inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            $controller('ChildController as vm', { $scope: scope });
        });
    });

    it('should be Child controller\'s myModel', function () {
        expect(scope.vm.myModel).toEqual('Child controller\'s myModel');
    });

    it('should be Child\'s variable', function () {
        expect(scope.vm.onlyChild).toEqual('Child\'s variable');
    });
});