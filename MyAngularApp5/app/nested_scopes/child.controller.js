(function () {

    angular.module('app')
        .controller('ChildController', ChildController);

    function ChildController() {
        var vm = this;

        vm.myModel = 'Child controller\'s myModel';
        vm.onlyChild = 'Child\'s variable';
    }

})();