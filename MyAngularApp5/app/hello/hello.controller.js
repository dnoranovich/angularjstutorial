(function () {

  angular.module('app')
    .controller('HelloController', HelloController);

  function HelloController() {
    var vm = this;

    vm.myModel = 'World';
  }

})();