describe('HelloController tests', function () {
    var HelloController;

    beforeEach(function () {
        module('app');

        inject(function ($controller) {
            HelloController = $controller('HelloController');
        });

    });

    it('should be World', function () {
        expect(HelloController.myModel).toEqual('World');
    });
});

describe('HelloController scope tests', function () {
    var scope;

    beforeEach(function () {
        module('app');

        inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            $controller('HelloController as vm', { $scope: scope });
        });

    });

    it('should be World', function () {
        expect(scope.vm.myModel).toEqual('World');
    });
});