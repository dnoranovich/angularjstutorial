
describe('Fun with numbers', function () {

    it('should return 4', function () {
        expect(2 + 2).toEqual(4);
    });

    it('should return 0', function () {
        expect(4 - 4).toEqual(0);
    });

    it('should be true', function () {
        expect(3 > 0).toBe(true);
    });

});

describe('Hello world', function () {
    var message = 'AngularJS';
    var input = element(by.model('vm.myModel'));

    beforeEach(function () {
        browser.get('http://localhost:3000');
    });

    it('should have title', function () {
        expect(browser.getTitle())
            .toEqual('Hello World AngularJS application');
    });

    it('should be world', function () {
        expect(input.getAttribute('value')).toEqual('World');
    });

    it('should be hello world', function () {
        expect(element(by.binding('vm.myModel')).getText())
            .toEqual('Hello World!');
    });

    it('should update greeting when data is entered in the input field',
        function () {
            input.clear().then(function () {
                input.sendKeys(message);
                expect(element(by.binding('vm.myModel')).getText())
                    .toEqual('Hello ' + message + '!');
            });
        });

});