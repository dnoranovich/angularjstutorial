describe('HelloController tests', function () {
    var scope;
    var HelloController;

    beforeEach(function () {
        module('app');

        inject(function ($controller) {
            scope = {};
            HelloController = $controller('HelloController', { $scope: scope });
        });

    });

    it('should be World', function () {
        expect(scope.myModel).toEqual('World');
    });
});