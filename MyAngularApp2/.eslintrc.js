module.exports = {
    "extends": ["angular", "eslint:recommended"],
    "plugins":["jasmine", "protractor"],
    "env":{
        "jasmine":true,
        "protractor":true,
        "node":true
    }
};